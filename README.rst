.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=====================
Le Filament - Projets
=====================

This module depends upon *hr_timesheet* and *hr_expense* modules.

This module provides:
 - the calculation of imputed hours and costs on the project
 - the project estimate (based on a variable of the number of hours per day)
 - a progressbar spent / budget
 - prospecting hours (new field to set up at the project level and based on the number of hours charged to a task named Prospection)


Credits
=======

Contributors ------------

* Benjamin Rivier <benjamin@le-filament.com>
* Remi Cazenave <remi@le-filament.com>
* Juliana Poudou <juliana@le-filament.com>


Maintainer ----------

.. image:: https://le-filament.com/img/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament